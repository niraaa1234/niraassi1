## Instructions on How to build The Gamers' Blog

1. Finding your Niche
2. Think of Unique Blog Name and Create Website
3. Get Essentials

## Instructions on How to install The Gamers' Blog

1. Select game to install.
2. In the popup menu, be sure to select new location in the "Choose Location for install" menu.
3. Install game as usual. Download speed will depend on upon your net connection.

## Instructions on How to Use The Gamers' Blog

1. The basic information is provided in the blog.
2. By subscribing the blog, you will get each and every updates of the game.
3. You can contact each other.
4. writing comments section has been given.
